package org.spiralarms.TreeViewer;

import com.company.AbstractTreeTableModel;
import com.company.TreeTableModel;

public class MyTreeModel extends AbstractTreeTableModel {
    static protected String[]  cNames = {"Name", "Speakers", "Room", "Time"};
    static protected Class[]  cTypes = {TreeTableModel.class, String.class, String.class, Integer.class};

    public MyTreeModel(Object root) {
        super(root);
    }


    @Override
    public int getColumnCount() {
        return cNames.length;
    }

    @Override
    public String getColumnName(int column) {
        return cNames[column];
    }

    public Class getColumnClass(int column) {
        return cTypes[column];
    }

    protected Object[] getChildren(Object node) {
        MyTreeNode treeNode = ((MyTreeNode)node);
        return treeNode.getChildren();
    }

    @Override
    public Object getValueAt(Object node, int column) {
        MyTreeNode treeNode = (MyTreeNode) node;
        try {
            switch(column) {
                case 0:
                    return treeNode.getName();
                case 1:
                    return treeNode.getSpeakers();
                case 2:
                    return treeNode.getRoom();
                case 3:
                    return treeNode.getTime();
            }
        }
        catch  (SecurityException ignored) { }

        return null;
    }

    @Override
    public Object getChild(Object node, int i) {
        return getChildren(node)[i];
    }

    @Override
    public int getChildCount(Object node) {
        Object[] children = getChildren(node);
        return (children == null) ? 0 : children.length;
    }
}
