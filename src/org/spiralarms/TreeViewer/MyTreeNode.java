package org.spiralarms.TreeViewer;

import java.io.Serializable;
import java.util.ArrayList;

public class MyTreeNode implements Serializable {
    private String name;
    private String speakers = "";
    private String room = "";
    private String time = "";
    private final ArrayList<MyTreeNode> children = new ArrayList<>();

    public MyTreeNode(String name) {
        this.name = name;
    }

    public MyTreeNode(String name, String speakers, String room, String time) {
        this.name = name;
        this.speakers = speakers;
        this.room = room;
        this.time = time;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSpeakers(String speakers) {
        this.speakers = speakers;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getName() {
        return name;
    }

    public String getSpeakers() {
        return speakers;
    }

    public String getRoom() {
        return room;
    }

    public String getTime() {
        return time;
    }

    @Override
    public String toString() {
        return name;
    }

    public void add(MyTreeNode child) {
        children.add(child);
    }

    public Object[] getChildren() {
        return children.toArray();
    }
}
