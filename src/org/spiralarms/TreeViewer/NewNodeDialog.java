package org.spiralarms.TreeViewer;

import javax.swing.*;

public class NewNodeDialog {
    private final String name, speakers, room, time;
    private final int result;

    public NewNodeDialog(String dialogTitle) {
        this(dialogTitle, "", "", "", "");
    }

    public NewNodeDialog(String dialogTitle, String name, String speakers, String room, String time) {
        JTextField fieldName = new JTextField();
        JTextField fieldSpeakers = new JTextField();
        JTextField fieldRoom = new JTextField();
        JTextField fieldTime = new JTextField();
        fieldName.setText(name);
        fieldSpeakers.setText(speakers);
        fieldRoom.setText(room);
        fieldTime.setText(time);
        final JComponent[] inputs = new JComponent[]{
                new JLabel("Name"),
                fieldName,
                new JLabel("Speakers"),
                fieldSpeakers,
                new JLabel("Room"),
                fieldRoom,
                new JLabel("Time"),
                fieldTime
        };
        this.result = JOptionPane.showConfirmDialog(null, inputs, "Edit node", JOptionPane.DEFAULT_OPTION);
        this.name = fieldName.getText();
        this.speakers = fieldSpeakers.getText();
        this.room = fieldRoom.getText();
        this.time = fieldTime.getText();
    }

    public int getResult() {
        return result;
    }

    public MyTreeNode getNode() {
        return new MyTreeNode(name, speakers, room, time);
    }
}
