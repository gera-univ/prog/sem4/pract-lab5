package org.spiralarms.TreeViewer;

import com.company.JTreeTable;
import com.company.TreeTableModel;
import com.company.TreeTableModelAdapter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;

public class TreeViewer {
    public static void main(String[] args) {
        new TreeViewer();
    }
    private MyTreeNode rootNode = new MyTreeNode("Conference");
    private JTreeTable treeTable;

    public TreeViewer() {
        JFrame frame = new JFrame("Tree Viewer");
        MyTreeNode rootNode = new MyTreeNode("Conference");
        treeTable = new JTreeTable(new MyTreeModel(rootNode));

        final JMenuBar menuBar = new JMenuBar();

        JMenu fileMenu = new JMenu("File");
        JMenu editMenu = new JMenu("Edit");
        JMenuItem exitMenuItem = new JMenuItem("Exit");
        JMenuItem saveMenuItem = new JMenuItem("Save");
        JMenuItem openMenuItem = new JMenuItem("Open");
        JMenuItem addChildMenuItem = new JMenuItem("Add child");
        JMenuItem editNodeMenuItem = new JMenuItem("Edit node");

        saveMenuItem.addActionListener(actionEvent -> {
            try {
                JFileChooser fileChooser = new JFileChooser();
                int option = fileChooser.showSaveDialog(frame);
                if (option == JFileChooser.APPROVE_OPTION){
                    FileOutputStream fileOut = new FileOutputStream(fileChooser.getSelectedFile());
                    ObjectOutputStream out = new ObjectOutputStream(fileOut);
                    out.writeObject(rootNode);
                    out.close();
                    fileOut.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        openMenuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    JFileChooser fileChooser = new JFileChooser();
                    int option = fileChooser.showOpenDialog(frame);
                    if (option == JFileChooser.APPROVE_OPTION){
                        FileInputStream fileIn = new FileInputStream(fileChooser.getSelectedFile());
                        ObjectInputStream in = new ObjectInputStream(fileIn);
                        setRootNode((MyTreeNode) in.readObject());
                        in.close();
                        fileIn.close();
                    }
                } catch (IOException | ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });


        exitMenuItem.addActionListener(actionEvent -> System.exit(0));

        addChildMenuItem.addActionListener(actionEvent -> {
            NewNodeDialog dialog = new NewNodeDialog("Add new node");
            if (dialog.getResult() == JOptionPane.OK_OPTION) {
                TreeTableModelAdapter adapter = treeTable.getModelAdapter();
                MyTreeNode parentNode = (MyTreeNode) adapter.nodeForRow(treeTable.getSelectedRow());
                MyTreeNode newNode = dialog.getNode();
                parentNode.add(newNode);
                treeTable.repaint();
            }
        });

        editNodeMenuItem.addActionListener(actionEvent -> {
            TreeTableModelAdapter adapter = treeTable.getModelAdapter();
            MyTreeNode node4 = (MyTreeNode) adapter.nodeForRow(treeTable.getSelectedRow());
            NewNodeDialog dialog = new NewNodeDialog("Edit existing node", node4.getName(), node4.getSpeakers(), node4.getRoom(), node4.getTime());

            if (dialog.getResult() == JOptionPane.OK_OPTION) {
                MyTreeNode sampleNode = dialog.getNode();
                node4.setName(sampleNode.getName());
                node4.setSpeakers(sampleNode.getSpeakers());
                node4.setRoom(sampleNode.getRoom());
                node4.setTime(sampleNode.getTime());
                treeTable.repaint();
            }
        });

        fileMenu.add(exitMenuItem);
        fileMenu.add(saveMenuItem);
        fileMenu.add(openMenuItem);
        editMenu.add(addChildMenuItem);
        editMenu.add(editNodeMenuItem);
        menuBar.add(fileMenu);
        menuBar.add(editMenu);

        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent we) {
                System.exit(0);
            }
        });

        frame.setJMenuBar(menuBar);
        frame.getContentPane().add(new JScrollPane(treeTable));
        frame.pack();
        frame.setVisible(true);
    }

    public void setRootNode(MyTreeNode rootNode) {
        this.rootNode = rootNode;
        treeTable.setTreeTableModel(new MyTreeModel(this.rootNode));
    }
}